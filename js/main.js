jQuery(function($){

  
	var App = {
		init: function() {
			this.setupElements();
			this.bindEvents();
		},

		setupElements: function() {
			this.$pageHeight = $(window);
			this.$preload = $(window);
			this.$responsiveJS = $(window);
			this.$showSearch = $('#searchIcon');
			this.$toggleBackground = $('#backgroundSelector div');
			this.$closeSearch = $('#searchPanel img');
		},			

		bindEvents: function() {
			App.$showSearch.on('click', this.showSearch);		
			App.$pageHeight.on('load', this.pageHeight);
			App.$pageHeight.on('resize', this.pageHeight);
			App.$preload.on('load', this.preload);
			App.$toggleBackground.on('click', this.toggleBackground);
			App.$closeSearch.on('click', this.hideSearch);
			App.$responsiveJS.on('load', this.responsiveJS);
			App.$responsiveJS.on('resize', this.responsiveJS);
		},

		pageHeight: function() {
			var mainWrap = $('#mainWrap');

			$(mainWrap).css({'height':'0'});
		  $(mainWrap).css({'height':($(document).height())+'px'});
		},

		preload: function() {
			var body = ('body');

			$(body).append('<img style="display: none;" src="img/MountainsBackground2.png">');
		},

		showSearch: function() {
			var searchPanel = $('#searchPanel');

			$(searchPanel).slideDown('fast');
		},

		hideSearch: function() {
			var searchPanel = $('#searchPanel');

			$(searchPanel).slideUp('fast');
		},

		toggleBackground: function() {
			var backgroundSelectorDiv = $('#backgroundSelector div')
			var body = $('body');

			$(backgroundSelectorDiv).removeClass('buttonActive');
			$(this).addClass('buttonActive');

			if($(this).is('div[data-background="slide1"]')) {

				$(body).removeClass('bodySlide2').addClass('bodySlide1');
				console.log('slide1');
			} 
			else {
				$(body).removeClass('bodySlide1').addClass('bodySlide2');
				console.log('slide2');
			}
		},

		responsiveJS: function() {
			var body = $('body');
			var backgroundSelectorDiv = $('#backgroundSelector div')

			if($(body).width() < 700) {
				$(body).removeClass('bodySlide1 bodySlide2').addClass('mobileBody');
				
				$("#mainNav").mmenu({
         	offCanvas: {
	          position: "right",
	        },
	        slidingSubmenus: false

	      });

			}
			else {
				$(body).removeClass('mobileBody');
				if($('div[data-background="slide1"]').hasClass('buttonActive')){
					$(body).addClass('bodySlide1');
				}
				if($('div[data-background="slide2"]').hasClass('buttonActive')){
					$(body).addClass('bodySlide2');
				}
			}
		},
		
	};

	App.init();
	window.App = App;
});